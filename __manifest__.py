# -*- coding: utf-8 -*-
{
    'name': "Registro libros",  # Titulo del módulo
    'summary': "Registro de libros y sus autores",  # Resumen de la funcionaliadad
    'description': 'Guarda un registro de los libros del almacen y los autores que los escribieron',  

    #Indicamos que es una aplicación
    'application': True,
    'author': "Brahyan Marin Pulgarin",
    'website': "https://site.educa.madrid.org/ies.infantaelena.galapagar/",
    'category': 'Warehouse Management',
    'license':'GPL-3',
    'version': '1.0',
    'depends': ['base'],
    'installable' : True,
    'maintainer' : 'Brahyan Marin Pulgarin',
    'auto_install': False,
    'external_dependencies' : {} ,
    'assets' : {},

    'data': [
       
        #Estos dos primeros ficheros:
        #1) El primero indica grupo de seguridad basado en rol
        #2) El segundo indica la politica de acceso del modelo
        #Mas información en https://www.odoo.com/documentation/14.0/es/developer/howtos/rdtraining/05_securityintro.html
        #Y en www.odoo.yenthevg.com/creating-security-groups-odoo/      
        'security/groups.xml',
        'security/ir.model.access.csv',
        
        #Cargamos los ficheros con vistas tanto de biblioteca_comic como de biblioteca_comic_categoria
        'views/biblioteca_comic.xml',
        'views/biblioteca_comic_categoria.xml'
    ],
    # Fichero con data de demo si se inicializa la base de datos con "demo data" (No incluido en ejemplo)
    # 'demo': [
    #     'demo.xml'
    # ],
}
